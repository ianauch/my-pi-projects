# Pig Latin Translator project example
print "Welcome to the Pig Latin Translator!"
pyg = 'ay'

# Get user input
original = raw_input('Enter a word:')

# Check to make sure user entered something and it is aplha only
if len(original) > 0 and original.isalpha():
    print original
    # Lowercase word and grab forst letter
    word = original.lower()
    first = word[0]
    # Check if word is vowel or consonant
    if first == "a" or first == "e" or first == "i" or first == "o" or first =="u":
        # If vowel just add "ay" to end
        new_word = word + pyg
        print new_word
    else:
        # If consonant move first letter to end of word and add "ay"
        word_length = len(word)
        new_word = word[1:word_length] + first + pyg
        print new_word

else:
	print 'empty'